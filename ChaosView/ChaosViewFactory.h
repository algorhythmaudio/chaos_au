#import <Cocoa/Cocoa.h>
#import <AudioUnit/AUCocoaUIView.h>



@class ChaosView;


@interface ChaosViewFactory : NSObject <AUCocoaUIBase> {
    IBOutlet ChaosView *uiView;
}

- (NSString *) description;	// string description of the view

@end
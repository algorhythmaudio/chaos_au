            ==================================================================
            ==================================================================
            ================ Audio Unit Plugin Template ======================
            ==================================================================
            ==================================================================

Notes:
    - There are two warnings when you start that can be ignored.

What MUST be customized:
    - Globally (these changes are not manditory)
        - rename everything that has ProjName in it.  Including the Xcode
          project file itself.
        - make sure to update the info.plist path in each target if you change
          the name of the plist file.
        - Update the path of the .pch (pre-compiled headers) in each target.
        - If you change the file name of AUTemplateViewFactory.h, in the xib you
          must set the File's Owner > Custom Class to the new name.  Make sure
          that the instance uiView has its outlet linked to the custom NSView.
    - Parameters.h
        - kVERSION: must be a hex code.  eg: 29.33.40 = 0x01d2128
        - kBundleName: replace ProjName with whatever name you change the Target
                       name to (see Target below).
        - kViewName: if you changed the name of the xib file, also change this 
                     macro.
        - kViewClass: if you change the class in AUTemplateViewFactory.h, also
                      change this name to match.
    - AUTemplate.cpp
        - dsp custom code (Process)
    - AUTemplatePlugin.r and AUTemplate-Info.plist
        - COMP_TYPE : must be one of the types listed in AUComponent.h
        - COMP_SUBTYPE : can be a subtype defined in AUComponent.h or
                         be a custom 4 char code.  If using an apple
                         defined subtype it should be similar to like
                         types
        - COMP_MANUF : a unique 4 char code that is registered with
                       apple.  This will be the same for all AU's
        - VERSION : must be the same as kVERSION
        - NAME : a string of the name of the plugin.  Convention has
                 the manuf like this: "AlgoRhythm Audio : My plugin".
        - DESCRIPTION : a description
    - Target
        - Click on the project file and then on the AU target (eg: ProjName). In the
          build settings under Packaging change the Product Name to be some name
          of the new plugin.  This is not something that the user will see or really
          interact with.  It just changes the name of the component file.  
          
Customizing:
    - Parameters
        - declaring ~ in the pch header file add the parameter to the enum.
        - properties ~ in the pch header file you assign a name, min, max, and default
                       values.
        - setup ~ in the controller file in the plugin constructor method, use the function:
                  SetParameter() to assign the parameter with its default values.
        - get info method ~ in the controller file, the GetParameterInfo() method there's
                            a swich that you add a case for the parameter which does the
                            following: sets the name, the unit of measure, and the min,
                            max, and default values.
    - Presets
        - declaring ~ in dsp header file, add a macro to the enum
        - naming ~ in the header file add a CFSTR() name within the AUPreset variable
        - factory default ~ in header file change the kPresetDefault to the desired 
                            preset.
        - setting ~ in the controller file, the method NewFactoryPresetSet(), add
                    the new preset macro to the switch and using SetParameter() set
                    each of the desired parameters.  
        - IMPORTANT ~ in the NewFactoryPresetSet() method, the if statement MUST contain
                      all of the presets.
                      
    - Custom View
        - generic/custom ~ in Parameters.h setting kHasCustomView to 0 will load the generic
                         view while setting it to 1 will use your custom view
        

Installing:
    - the product (.component) must live in /Library/Audio/Plug-Ins/Components
    
Testing:
    Note: building the project will copy a temp component to the proper folder. So
          the first part can be skipped while programming, but it shouldn't when
          preparing to release an AU for beta testing or for its release.
          
    - After copying the component into the correct folder: open Terminal and type:
                
                            auval -a

      This will print all the available AU's in the component folder.  Assuming you 
      see the new plugin, test it by typing:
      
                            auval -v subtype type manuf
                            
      To test the 64 bit version type:
      
                            auval -64 -v subtype type manuf
                            
      NOTE: for subtype put the subtype, type for type, manuf for manuf. eg: aufx DEVL Algo
      
    - Next you can test it with AULab.  Just hit Run and it will open AULab then you can add
      a generator track to play an audio file.  Nex you add an instance of the new
      plugin to hear it work.  Any logs will be printed to the console in Xcode if using
      this method.





//
//  AAChaos.h
//  ChaosPrototype
//
//  Created by GW on 10/7/12.
//  Copyright (c) 2012 Algorhythm_Audio. All rights reserved.
//

#ifndef __ChaosPrototype__AAChaos__
#define __ChaosPrototype__AAChaos__

#include <iostream>
#include "AADsp.h"


#define kSmoothingDefault 1024;




typedef struct chaosVars {
    double x, y, z;
} ChaosVars;


class AAChaos {
        
public:
    ChaosVars   *chaos;
    bool interpComplete;
    
    AAChaos();
    ~AAChaos();
    
    // First you must call chaosTick(), then call interpChaos() only if interpComplete == NO
    void            chaosTick();
    void            interpChaos();

    
    // Getters
    ChaosVars*      getChaos()      {return chaos;}
    double          getPrandt()     {return p;}
    double          getRayleigh()   {return r;}
    double          getConstant()   {return b;}
    double          getScale()      {return scale;}
    
    // Setters
    void            setPrandt       (double prandt);
    void            setRayleigh     (double rayleigh);
    void            setConstant     (double constant);
    void            setAllParams    (double prandt, double rayleigh, double constant);
    
    void            setScale        (double thisScale);
    void            setSmoothing    (unsigned long thisSmoothing);
    
    // Other
    void            printChaos();
    void            reset(); // used to stablize the vaiables to their netural state
    
    
private:
    
    double          p, r, b; // user can change these values
    double          scale;
    unsigned long   smoothing;
    unsigned long   currentSmoothing;
    
    double dx, dy, dz;
    
    ChaosVars *oldChaos;
    ChaosVars *newChaos;
    chaosVars *outputScale;
};


#endif /* defined(__ChaosPrototype__AAChaos__) */







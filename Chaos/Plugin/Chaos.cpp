#include "Chaos.h"




#pragma mark Create and Destroy

/* this is the constructor method.  this is where all the init happens */

ChaosPluginKernal :: ChaosPluginKernal(AUEffectBase *inAudioUnit) : AUKernelBase(inAudioUnit),
                                                                    filterHP(true),
                                                                    filterLP(true),
                                                                    filterBP(true),
                                                                    filterBR(true)
{
    sr = GetSampleRate();
    
    filterParams.set(paramID_Q, PARAM_DEFAULTS[paramID_Q]);
    filterParams.set(paramID_Sr, sr);
    
    filterBase = &filterHP;
    
    filterBase->setAllParams(filterParams);
    
    currentType = kParam3TypeHP;
    
    // prep chaos
    mod = new AAChaos;
    chaosRate = sr * kParam1Default;
    
    iChaosVars = (ChaosVars*) malloc(sizeof(ChaosVars));
    if (!iChaosVars) {
        puts("Error: couldn't alloc memory for iChaosVars");
        throw CAException(kAudioUnitErr_FailedInitialization);
    }
    
    // run chaos a few times
    for (int i=0; i<100; i++) mod->chaosTick();
    
    setChaosVars();
    panAngle = 0;
    distortion = waveShaper1; // must be set to something at first
}


// make sure to dealloc anything malloced here
ChaosPluginKernal::~ChaosPluginKernal() {
    
}


// here you define all the parameters that will be used
ChaosPlugin::ChaosPlugin(AudioUnit component) : AUEffectBase(component, false) {
    
    CreateElements();
    Globals() -> UseIndexedParameters(kParamNum);
    
    // set default parameters here
    SetParameter(kParam1_GlobalChaosRate, kParam1Default);
    SetParameter(kParam2_GlobalChaosScale, kParam2Default);
    SetParameter(kParam3_FilterType, kParam3Default);
    SetParameter(kParam4_FilterQ, kParam4_FilterQ);
    SetParameter(kParam5_FilterChaosScale, kParam5Default);
    SetParameter(kParam6_FilterChaosVar, kParam6Default);
    SetParameter(kParam7_DistType, kParam7Default);
    SetParameter(kParam8_DistChaosScale, kParam8Default);
    SetParameter(kParam9_DistChaosVar, kParam9Default);
    SetParameter(kParam10_PanChaosScale, kParam10Default);
    SetParameter(kParam11_PanChaosVar, kParam11Default);
    
    // this sets the factory preset, no need to change this
    SetAFactoryPresetAsCurrent(kPresets[kPresetDefault]);
}




// this can update the view if uninitialized (see FilterDemo)
OSStatus ChaosPlugin::Initialize() {
    OSStatus result = AUEffectBase::Initialize();
    
    if (result == noErr) {
        // here you add copy protection code
    }
    
    return result;
}


void ChaosPluginKernal::Reset() {
    // shouldn't need to reset anything here
    puts("Reset");
    
    filterBase->reset();
}




#pragma mark - Getters - Parameters

// this function sets different parts for each parameter
OSStatus ChaosPlugin::GetParameterInfo(AudioUnitScope			inScope,
                                       AudioUnitParameterID     inParameterID,
                                       AudioUnitParameterInfo	&outParameterInfo )
{
    ComponentResult result = noErr;
    
    // tells the host that all the parameters are readable and writable
    outParameterInfo.flags = kAudioUnitParameterFlag_IsReadable | kAudioUnitParameterFlag_IsWritable;
    
    if (inScope == kAudioUnitScope_Global) {
        switch (inParameterID) {
            case kParam1_GlobalChaosRate:
                AUBase::FillInParameterName(outParameterInfo, kParamName1, false);
                outParameterInfo.unit = kAudioUnitParameterUnit_Seconds;
                outParameterInfo.minValue = kParam1Min;
                outParameterInfo.maxValue = kParam1Max;
                outParameterInfo.defaultValue = kParam1Default;
                break;
            case kParam2_GlobalChaosScale:
                AUBase::FillInParameterName(outParameterInfo, kParamName2, false);
                outParameterInfo.unit = kAudioUnitParameterUnit_Generic;
                outParameterInfo.minValue = kParam2Min;
                outParameterInfo.maxValue = kParam2Max;
                outParameterInfo.defaultValue = kParam2Default;
                break;
            case kParam3_FilterType:
                AUBase::FillInParameterName(outParameterInfo, kParamName3, false);
                outParameterInfo.unit = kAudioUnitParameterUnit_Indexed;
                outParameterInfo.minValue = kParam3Min;
                outParameterInfo.maxValue = kParam3Max;
                outParameterInfo.defaultValue = kParam3Default;
                break;
            case kParam4_FilterQ:
                AUBase::FillInParameterName(outParameterInfo, kParamName4, false);
                outParameterInfo.unit = kAudioUnitParameterUnit_Generic;
                outParameterInfo.minValue = kParam4Min;
                outParameterInfo.maxValue = kParam4Max;
                outParameterInfo.defaultValue = kParam4Default;
                break;
            case kParam5_FilterChaosScale:
                AUBase::FillInParameterName(outParameterInfo, kParamName5, false);
                outParameterInfo.unit = kAudioUnitParameterUnit_Generic;
                outParameterInfo.minValue = kParam5Min;
                outParameterInfo.maxValue = kParam5Max;
                outParameterInfo.defaultValue = kParam5Default;
                break;
            case kParam6_FilterChaosVar:
                AUBase::FillInParameterName(outParameterInfo, kParamName6, false);
                outParameterInfo.unit = kAudioUnitParameterUnit_Indexed;
                outParameterInfo.minValue = kParam6Min;
                outParameterInfo.maxValue = kParam6Max;
                outParameterInfo.defaultValue = kParam6Default;
                break;
            case kParam7_DistType:
                AUBase::FillInParameterName(outParameterInfo, kParamName7, false);
                outParameterInfo.unit = kAudioUnitParameterUnit_Indexed;
                outParameterInfo.minValue = kParam7Min;
                outParameterInfo.maxValue = kParam7Max;
                outParameterInfo.defaultValue = kParam7Default;
                break;
            case kParam8_DistChaosScale:
                AUBase::FillInParameterName(outParameterInfo, kParamName8, false);
                outParameterInfo.unit = kAudioUnitParameterUnit_Generic;
                outParameterInfo.minValue = kParam8Min;
                outParameterInfo.maxValue = kParam8Max;
                outParameterInfo.defaultValue = kParam8Default;
                break;
            case kParam9_DistChaosVar:
                AUBase::FillInParameterName(outParameterInfo, kParamName9, false);
                outParameterInfo.unit = kAudioUnitParameterUnit_Indexed;
                outParameterInfo.minValue = kParam9Min;
                outParameterInfo.maxValue = kParam9Max;
                outParameterInfo.defaultValue = kParam9Default;
                break;
            case kParam10_PanChaosScale:
                AUBase::FillInParameterName(outParameterInfo, kParamName10, false);
                outParameterInfo.unit = kAudioUnitParameterUnit_Generic;
                outParameterInfo.minValue = kParam10Min;
                outParameterInfo.maxValue = kParam10Max;
                outParameterInfo.defaultValue = kParam10Default;
                break;
            case kParam11_PanChaosVar:
                AUBase::FillInParameterName(outParameterInfo, kParamName11, false);
                outParameterInfo.unit = kAudioUnitParameterUnit_Indexed;
                outParameterInfo.minValue = kParam11Min;
                outParameterInfo.maxValue = kParam11Max;
                outParameterInfo.defaultValue = kParam11Default;
                break;
            default:
                result = kAudioUnitErr_InvalidParameter;
                break;
        }
    }
    else
        result = kAudioUnitErr_InvalidParameter;
    
    return result;
}


/* This method is so the au can get the names of each of the indexed values only for kAudioUnitParameterUnit_Indexed.
   See RingMod for an example of an indexed type.
 */
OSStatus ChaosPlugin::GetParameterValueStrings(AudioUnitScope		inScope,
                                               AudioUnitParameterID	inParameterID,
                                               CFArrayRef			*outStrings)
{
    if (inScope == kAudioUnitScope_Global) {
        if (outStrings == NULL) return noErr;
        
        if (inParameterID == kParam3_FilterType) {
            CFStringRef strings[] = {kParam3TypeNameLP, kParam3TypeNameHP, kParam3TypeNameBP, kParam3TypeNameBR};
            *outStrings = CFArrayCreate(NULL, (const void**) strings, kParam3TypeNum, NULL);
            return noErr;
        }
        else if (inParameterID == kParam7_DistType) {
            CFStringRef strings[] = {kParam7TypeName1, kParam7TypeName2, kParam7TypeName3};
            *outStrings = CFArrayCreate(NULL, (const void**) strings, kParam7TypeNum, NULL);
            return noErr;
        }
        else if (inParameterID == kParam6_FilterChaosVar) {
            CFStringRef strings[] = {kChaosNameX, kChaosNameY, kChaosNameZ, kChaosNameNone};
            *outStrings = CFArrayCreate(NULL, (const void**) strings, kChaosNum, NULL);
        }
        else if (inParameterID == kParam9_DistChaosVar) {
            CFStringRef strings[] = {kChaosNameX, kChaosNameY, kChaosNameZ, kChaosNameNone};
            *outStrings = CFArrayCreate(NULL, (const void**) strings, kChaosNum, NULL);
        }
        else if (inParameterID == kParam11_PanChaosVar){
            CFStringRef strings[] = {kChaosNameX, kChaosNameY, kChaosNameZ, kChaosNameNone};
            *outStrings = CFArrayCreate(NULL, (const void**) strings, kChaosNum, NULL);
        }
        else
            return kAudioUnitErr_InvalidProperty;
    }
    
    return  kAudioUnitErr_InvalidProperty;
}




#pragma mark Properties


// no need to touch this
OSStatus ChaosPlugin::GetPropertyInfo(AudioUnitPropertyID    inID,
                                         AudioUnitScope			inScope,
                                         AudioUnitElement		inElement,
                                         UInt32                 &outDataSize,
                                         Boolean                &outWritable )
{
    if (inScope == kAudioUnitScope_Global) {
        switch (inID) {
            case kAudioUnitProperty_CocoaUI:
                if (kHasCustomView) {
                    puts("GetPropertyInfo call and ready for cocoa view");
                    outWritable = false;
                    outDataSize = sizeof(AudioUnitCocoaViewInfo);
                    return noErr;
                }
        }
    }
    
    return AUEffectBase::GetPropertyInfo(inID, inScope, inElement, outDataSize, outWritable);
}


// no need to touch this
OSStatus ChaosPlugin::GetProperty(AudioUnitPropertyID 	inID,
                                     AudioUnitScope 		inScope,
                                     AudioUnitElement 		inElement,
                                     void 					*outData )
{
    if (inScope == kAudioUnitScope_Global) {
        switch (inID) {
            case kAudioUnitProperty_CocoaUI: // allows host to find the custom view
                if (kHasCustomView) {
                    CFBundleRef bundle = CFBundleGetBundleWithIdentifier(kBundleName);
                    if (bundle == NULL) return fnfErr;
                    
                    CFURLRef bundleURL = CFBundleCopyResourceURL(bundle, kViewName, CFSTR("bundle"), NULL);
                    if (bundleURL == NULL) return fnfErr;
                    
                    CFStringRef className = kViewClass;
                    AudioUnitCocoaViewInfo cocoaInfo = {bundleURL, className};
                    *((AudioUnitCocoaViewInfo*) outData) = cocoaInfo;
                    
                    return noErr;
                }
        }
    }
    
    // if we've gotten this far, handles the standard properties
	return AUEffectBase::GetProperty (inID, inScope, inElement, outData);
}




#pragma mark Presets

// this function prepares the presets.  No need to touch this
OSStatus ChaosPlugin::GetPresets (CFArrayRef *outData) const {
    if (outData == NULL) return noErr;
    
    CFMutableArrayRef presetsArray = CFArrayCreateMutable(NULL, kPresetNum, NULL);
    
    for (int i=0; i<kPresetNum; i++)
        CFArrayAppendValue(presetsArray, &kPresets[i]);
    
    *outData = (CFArrayRef) presetsArray;
    return noErr;
}







#pragma mark - Utility


// define presets here.
OSStatus ChaosPlugin::NewFactoryPresetSet (const AUPreset &inNewFactoryPreset) {
    SInt32 choosenPreset = inNewFactoryPreset.presetNumber;
    
    // must include all possible presets
    if (kPreset1) {
        for (int i=0; i<kPresetNum; i++) {
            if (choosenPreset == kPresets[i].presetNumber) {
                switch (choosenPreset) {
                    case kPreset1:
                        SetParameter(kParam1_GlobalChaosRate, kParam1Default);
                        SetParameter(kParam2_GlobalChaosScale, kParam2Default);
                        SetParameter(kParam3_FilterType, kParam3Default);
                        SetParameter(kParam4_FilterQ, kParam4_FilterQ);
                        SetParameter(kParam5_FilterChaosScale, kParam5Default);
                        SetParameter(kParam6_FilterChaosVar, kParam6Default);
                        SetParameter(kParam7_DistType, kParam7Default);
                        SetParameter(kParam8_DistChaosScale, kParam8Default);
                        SetParameter(kParam9_DistChaosVar, kParam9Default);
                        SetParameter(kParam10_PanChaosScale, kParam10Default);
                        SetParameter(kParam11_PanChaosVar, kParam11Default);
                        break;
                }
                
                SetAFactoryPresetAsCurrent(kPresets[i]);
                return noErr;
            }
        }
    }
    
    return kAudioUnitErr_InvalidProperty;
}



#pragma mark - Private

void ChaosPluginKernal::prep() {
    // check filter type
    FilterType newType = (FilterType) GetParameter(kParam3_FilterType);
    if (currentType != newType) {
        switch (newType) {
            case kParam3TypeHP: filterBase = &filterHP; break;
            case kParam3TypeLP: filterBase = &filterLP; break;
            case kParam3TypeBP: filterBase = &filterBP; break;
            case kParam3TypeBR: filterBase = &filterBR; break;
            default: break;
        }
        currentType = newType;
    }
    
    // get parameters
    mod->setScale(static_cast<double>(GetParameter(kParam2_GlobalChaosScale) / 200.0));
    
    if (filterBase->getParam(paramID_Q) != GetParameter(kParam4_FilterQ))
        filterBase->setParam(paramID_Q, GetParameter(kParam4_FilterQ));
    
    chaosScaleFilter = GetParameter(kParam5_FilterChaosScale);
    chaosScaleDist = GetParameter(kParam8_DistChaosScale);
    chaosScalePan = GetParameter(kParam10_PanChaosScale);

    chaosVarFilter = GetParameter(kParam6_FilterChaosVar);
    chaosVarDist = GetParameter(kParam9_DistChaosVar);
    chaosVarPan = GetParameter(kParam11_PanChaosVar);
    
    setDistAmmount();
}


void ChaosPluginKernal::modulate() {
    mod->interpChaos();
    setChaosVars();
    switch (chaosVarFilter) {
        default:
        case 0: filterBase->setParam(paramID_Freq, ((iChaosVars->x * iChaosVars->x) * 20000.0) * chaosScaleFilter);     break;
        case 1: filterBase->setParam(paramID_Freq, ((iChaosVars->y * iChaosVars->y) * 20000.0) * chaosScaleFilter);     break;
        case 2: filterBase->setParam(paramID_Freq, ((iChaosVars->z * iChaosVars->z) * 20000.0) * chaosScaleFilter);     break;
    }

    setDistAmmount();
    
    // calc panning angle
    switch (chaosVarPan) {
        case 0: panAngle = static_cast<float>(((2.0 * atan(1.0)) * iChaosVars->x) * 0.5);                   break;
        case 1: panAngle = static_cast<float>(((2.0 * atan(1.0)) * iChaosVars->y) * 0.5);                   break;
        case 2: panAngle = static_cast<float>(((2.0 * atan(1.0)) * ((iChaosVars->z - 0.5) * 2.0)) * 0.5);   break;
    }
}


void ChaosPluginKernal::setDistAmmount() {
    // set distAmmount
    switch (chaosVarDist) {
        case 0: distAmmount = static_cast<float>(fabs(iChaosVars->x) * chaosScaleDist);     break;
        case 1: distAmmount = static_cast<float>(fabs(iChaosVars->y) * chaosScaleDist);     break;
        case 2: distAmmount = static_cast<float>(iChaosVars->z * chaosScaleDist);           break;
    }
    
    DistTypes dist = (DistTypes) GetParameter(kParam7_DistType);
    
    switch (dist) {
        default:
        case kParam7Type1:
            distortion = waveShaper1;
            distAmmount *= 10.0;
            break;
        case kParam7Type2:
            distortion = waveShaper3;
            if (distAmmount >= 1.0) distAmmount = 0.99;
            break;
        case kParam7Type3:
            distortion = foldDist1;
            distAmmount = 0.7 - distAmmount;
            
            if (distAmmount < 0.0) {distAmmount = 0.0; break;}
            else if (distAmmount > 1.0) {distAmmount = 1.0; break;}
            break;
    }
}


void ChaosPluginKernal::printMoulation() {
    //printf("freq: %.0f\n", filterBase->getParam(paramID_Freq));
    //printf("dist: %.4f\n", distAmmount);
    printf("pan: %.3f\n", panAngle);
}


// scale individual 
void ChaosPluginKernal::setChaosVars() {
    *iChaosVars = *mod->chaos;
    
    iChaosVars->x *= 0.0470; // vals range -0.972<x<0.975
    iChaosVars->y *= 0.0355; // vals range -0.992<y<0.995
    iChaosVars->z *= 0.0180; // vals range  0.042<z<0.944
    
    //printf("x: %.3f\ty: %.3f\tz: %.3f\n", iChaosVars->x, iChaosVars->y, iChaosVars->z);
}




#pragma mark - Pre Process



OSStatus ChaosPlugin::ProcessBufferLists(AudioUnitRenderActionFlags    &ioActionFlags,
                                              const AudioBufferList         &inBuffer,
                                              AudioBufferList               &outBuffer,
                                              UInt32                        inFramesToProcess)
{
    // first check for silence and bypass if true
	bool silentInput = IsInputSilent (ioActionFlags, inFramesToProcess);
	ioActionFlags |= kAudioUnitRenderAction_OutputIsSilence;
    
    if (silentInput)
        return noErr;
    else
        ioActionFlags &= ~kAudioUnitRenderAction_OutputIsSilence;
    
    
    if (inBuffer.mNumberBuffers == 1) {
        for (size_t i=0; i<mKernelList.size(); i++) {
            ChaosPluginKernal *kernel = (ChaosPluginKernal*) mKernelList[i];
            
            if (kernel == NULL) continue;
            
            kernel->dspMono((const Float32*) inBuffer.mBuffers[0].mData + i,
                            (Float32*) outBuffer.mBuffers[0].mData + i,
                            inFramesToProcess,
                            i);
        }
    }
    else {
        for (size_t i=0; i<mKernelList.size(); i++) {
            ChaosPluginKernal *kernel = (ChaosPluginKernal*) mKernelList[i];
            
            if (kernel == NULL) continue;
            
            kernel->dspMono((const Float32*) inBuffer.mBuffers[i].mData,
                            (Float32*) outBuffer.mBuffers[i].mData,
                            inFramesToProcess,
                            i);
        }
    }

    
    
    return noErr;
}






#pragma mark - DSP


void ChaosPluginKernal :: dspMono(const Float32     *inSourceP,
                                  Float32           *inDestP,
                                  UInt32            inFramesToProcess,
                                  size_t            channel)

{
    // first build local buffers
    Float32 *input = (Float32*) inSourceP;
    Float32 *output = inDestP;
    
    // get parameters
    prep();
    printMoulation();
    
    // DSP
    for (int i=0; i<inFramesToProcess; i++, chaosRate--) {
        if (chaosRate <= 0) {
            chaosRate = sr * GetParameter(kParam1_GlobalChaosRate);
            mod->chaosTick();
        }
        
        if (mod->interpComplete == NO) modulate();
        
        // Filter
        if (chaosVarFilter != kChaosNone) filterBase->process(input, input, 1);
        
        // Distortion
        if (chaosVarDist != kChaosNone) *input = distortion(input, distAmmount);
        
        // panning
        if (chaosVarPan != kChaosNone) {
            switch (channel) {
                case 0:
                    *input *= ROOT2OVR2 * (cosf(panAngle) - sinf(panAngle)); // left
                    break;
                case 1:
                    *input *= ROOT2OVR2 * (cosf(panAngle) + sinf(panAngle)); // right
                    break;
            }
        }
        *output++ = *input++;
    }
}















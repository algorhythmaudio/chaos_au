#ifndef __Chaos__Chaos__
#define __Chaos__Chaos__


#include <AudioToolbox/AudioUnitUtilities.h>
#include "AUEffectBase.h"

#include "AAChaos.h"
#include "AADistortion.h"
#include "AAFilterLibrary.h"


// This is in ms and for any effect that continues to process after all audio
// packets have been received.
#define kAllowedTailTime (Float64) 0.0 // default is 0





class ChaosPluginKernal : public AUKernelBase {
    
    
private:
    
    Float64                 sr;
    
    // chaos ivar
    AAChaos                 *mod;
    chaosVars               *iChaosVars;
    int                     chaosRate;
    int                     chaosRateCounter;
    
    int chaosVarFilter;
    int chaosVarDist;
    int chaosVarPan;
    
    Float32 chaosScaleFilter;
    Float32 chaosScaleDist;
    Float32 chaosScalePan;
    
    // Filter ivars

    AADsp::AAFilter::AAFilterBase           *filterBase;
    AAFilterParams                          filterParams;
    
    AADsp::AAFilter::IIRBiquad<RBJ::HP>     filterHP;
    AADsp::AAFilter::IIRBiquad<RBJ::LP>     filterLP;
    AADsp::AAFilter::IIRBiquad<RBJ::BP1>    filterBP;
    AADsp::AAFilter::IIRBiquad<RBJ::BR>     filterBR;
    
    FilterType              currentType;
    
    // Distortion ivars
    dist_sample             distortion;
    float                   distAmmount;
    
    // Panning
    float                   panAngle;
    
    // Private helpers
    
    void                    prep();
    void                    modulate();
    void                    printMoulation();
    void                    setDistAmmount();
    
    void                    setChaosVars();

    
public:
    
    ChaosPluginKernal (AUEffectBase *inAudioUnit);
    virtual ~ChaosPluginKernal ();
    virtual void    Reset();
    
    // independent channel processing
    virtual void            dspMono     (const Float32  *inSourceP,
                                         Float32        *inDestP,
                                         UInt32			inFramesToProcess,
                                         size_t         channel);
};


class ChaosPlugin : public AUEffectBase {
    
    
public:
    ChaosPlugin (AudioUnit component);
    
    virtual AUKernelBase *NewKernel() {return new ChaosPluginKernal(this);}
    
    virtual OSStatus Version() {return kVERSION;}
    virtual OSStatus Initialize();
    
    // parameter
    virtual OSStatus GetParameterInfo           (AudioUnitScope			inScope,
                                                 AudioUnitParameterID   inParameterID,
                                                 AudioUnitParameterInfo	&outParameterInfo );
    virtual OSStatus GetParameterValueStrings   (AudioUnitScope         inScope,
                                                 AudioUnitParameterID	inParameterID,
                                                 CFArrayRef *			outStrings);
    
    // property
    virtual OSStatus GetPropertyInfo            (AudioUnitPropertyID    inID,
                                                 AudioUnitScope			inScope,
                                                 AudioUnitElement		inElement,
                                                 UInt32                 &outDataSize,
                                                 Boolean                &outWritable );
    
    virtual OSStatus GetProperty                (AudioUnitPropertyID 	inID,
                                                 AudioUnitScope 		inScope,
                                                 AudioUnitElement 		inElement,
                                                 void 					*outData );
    
    
    virtual OSStatus GetPresets                 (CFArrayRef *outData) const;
    virtual OSStatus NewFactoryPresetSet        (const AUPreset &inNewFactoryPreset);
    
    virtual void SetMaxFramesPerSlice           (UInt32 nFrames) {AUBase::SetMaxFramesPerSlice(nFrames);};
    virtual	bool SupportsTail () { return true; } // this should always be true for effects
    virtual Float64 GetTailTime() {return kAllowedTailTime;}
    
    
    // This is the dsp engine
    virtual OSStatus ProcessBufferLists(AudioUnitRenderActionFlags &ioActionFlags,
                                        const AudioBufferList &inBuffer,
                                        AudioBufferList &outBuffer,
                                        UInt32 inFramesToProcess);
    
private:
    
    
    
};




AUDIOCOMPONENT_ENTRY(AUBaseFactory, ChaosPlugin);





// Presets
enum {
    kPreset1 = 0,
    kPresetNum
};

static AUPreset kPresets [kPresetNum] = {
    {kPreset1, CFSTR("Default")},
};

// factory default
static const int kPresetDefault = kPreset1;














#endif /* defined(__Chaos__Chaos__) */

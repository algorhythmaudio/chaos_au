#include "AAChaos.h"

using namespace std;

#pragma mark Life


AAChaos :: AAChaos() {
    chaos = (ChaosVars*) malloc(sizeof(ChaosVars));
    oldChaos = (ChaosVars*) malloc(sizeof(ChaosVars));
    newChaos = (ChaosVars*) malloc(sizeof(ChaosVars));
    
    if (chaos == NULL || oldChaos == NULL || newChaos == NULL) {
        puts("Couldn't init AAChaos vaiables");
        this->~AAChaos(); // is this right???
    }
    else {
        // set ivars
        p = 10.0;
        r = 28.0;
        b = 2.67;
        scale = 0.01;
        
        reset();
        
        oldChaos = chaos;
        newChaos = chaos;
        
        interpComplete = YES;
        smoothing = kSmoothingDefault;
        currentSmoothing = 0;
    }
    
}


AAChaos :: ~AAChaos() {
    if (chaos) free(chaos);
}





#pragma mark - Setters



void AAChaos :: setPrandt (double prandt) {
    if (prandt <= 0) return;
    p = prandt;
}


void AAChaos :: setRayleigh (double rayleigh) {
    if (rayleigh <= 0) return;
    r = rayleigh;
}


void AAChaos :: setConstant (double constant) {
    if (constant <= 0) return;
    b = constant;
}


void AAChaos :: setAllParams (double prandt, double rayleigh, double constant) {
    if (prandt <=0 || rayleigh <= 0 || constant <= 0) return;
    
    p = prandt;
    r = rayleigh;
    b = constant;
}


void AAChaos :: setScale (double thisScale) {
    if (thisScale <= 0) return;
    scale = thisScale;
}


void AAChaos :: setSmoothing(unsigned long thisSmoothing) {
    if (thisSmoothing < 1)
        smoothing = 1;
    else
        smoothing = thisSmoothing;
}








#pragma mark - Other

void AAChaos :: printChaos() {
    printf("x = %f ", chaos->x);
    printf("y = %f ", chaos->y);
    printf("z = %f\n", chaos->z);
}


void AAChaos :: reset() {
    chaos->x = 0.1;
    chaos->y = 0.1;
    chaos->z = 0.1;
}







#pragma mark - Generators


void AAChaos :: chaosTick() {
    //puts("tick");
    // save old params and stop interp
    interpComplete = YES;
    oldChaos = chaos;
    
    // calc chaos
    dx = p * (oldChaos->y - oldChaos->x);
    dy = (r*oldChaos->x) - oldChaos->y - (oldChaos->x * oldChaos->z);
    dz = (oldChaos->x * oldChaos->y) - (b * oldChaos->z);
    
    newChaos->x += dx*scale;
    newChaos->y += dy*scale;
    newChaos->z += dz*scale;
    
    currentSmoothing = 0;
    interpComplete = NO;
}


void AAChaos :: interpChaos() {
    if (currentSmoothing >= smoothing || interpComplete == YES) {
        interpComplete = YES;
        return;
    }
    // y=mx+b, where oldChaos = (x1=0, y1), newChaos = (x2=smoothing, y2)
    
    // solve for m=(y2-y1)/(x2-x1)
    double m = (newChaos->x - oldChaos->x) / smoothing;
    
    // b = oldChoas->x
    chaos->x = (m * currentSmoothing) + oldChaos->x;
    
    m = (newChaos->y - oldChaos->y) / smoothing;
    chaos->y = (m * currentSmoothing) + oldChaos->y;
    
    m = (newChaos->z - oldChaos->z) / smoothing;
    chaos->z = (m * currentSmoothing) + oldChaos->z;
    
    currentSmoothing++;
}
























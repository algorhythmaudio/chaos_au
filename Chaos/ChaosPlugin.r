#include <AudioUnit/AudioUnit.r>


#define RES_ID          1000
#define COMP_TYPE       'aufx'
#define COMP_SUBTYPE    'Chos'
#define COMP_MANUF      'Algo'
#define VERSION         0x000181
#define NAME            "AlgoRhythm Audio: Chaos"
#define DESCRIPTION     "Chaos modulation"
#define ENTRY_POINT     "ChaosPluginEntry"

#include "AUResources.r"